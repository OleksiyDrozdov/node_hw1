const express = require('express');
const {
  createFile,
  getFiles,
  getFileByName
} = require('./controller/controller');
const rout = express.Router();
rout.post('/', createFile);
rout.get('/', getFiles);
rout.get('/:filename', getFileByName);
module.exports = rout;
