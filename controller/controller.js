const path = require('path');
const fs = require('fs');
const API_PATH = '../files';
const dir = './files';

!fs.existsSync(dir) && fs.mkdirSync(dir);

exports.createFile = function (req, res) {
  const {
    filename,
    content
  } = req.body;

  const re = /[^.*]{1,}\.(txt|log|json|yaml|xml|js)$/gm;
  const formatFile = re.test(filename);

  if (!formatFile || !content) {
    return res.status(400).json({
      message: 'Please specify "content" parameter'
    });
  };

  const fileP = path.join(__dirname, API_PATH, filename);
  fs.writeFile(fileP, content, (err) => {
    if (err) {
      return res.status(500).json({
        message: 'Server error'
      });
    };
    res.status(200).json({
      message: 'File created successfully'
    });
  });
};

exports.getFiles = function (req, res) {
  const dirP = path.join(__dirname, API_PATH);
  fs.readdir(dirP, (err, data) => {
    if (err) {
      return res.status(500).json({
        message: 'Server error'
      });
    };
    res.status(200).json({
      message: 'Success',
      data
    });
  });
};

exports.getFileByName = function (req, res) {
  const {
    filename
  } = req.params;
  const fileP = path.join(__dirname, API_PATH, filename);
  fs.readFile(fileP, 'utf-8', (err, content) => {
    if (err) {
      if (err.code === 'ENOENT') {
        return res.status(400).json({
          message: `No file with ${filename} filename found`
        });
      };
      return res.status(500).json({
        message: 'Server error'
      });
    };
    res.status(200).json({
      message: 'Success',
      filename,
      content,
      extension: path.extname(filename),
      uploadedDate: fs.statSync(fileP).birthtime
    });
  });
};
