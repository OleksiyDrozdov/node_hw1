const express = require('express');
const routerFile = require('./routerFile');
const morgan = require('morgan');

const app = express();
app.use(express.json());
app.use(morgan('short'));
app.use('/api/files', routerFile);

app.listen(8080, () => {
  console.log(`Server started on port 8080...`);
});